package com.example.Identity.service;

import com.example.Identity.dao.UserDao;
import com.example.Identity.enums.UserStatus;
import com.example.Identity.model.User;
import com.example.Identity.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public User save(UserDTO user) {
        User newUser = new User();
        newUser.setEmail(user.getEmail());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setFirstName(user.getFirstName());
        newUser.setMiddleName(user.getMiddleName());
        newUser.setLastName(user.getLastName());
        newUser.setBirthday(user.getBirthday());
        newUser.setStatus(UserStatus.ACTIVE);
        return userDao.save(newUser);
    }
    public Iterable<User> getUsers() {
        return userDao.findAll();
    }

    public String updateUser(@PathVariable long id, @RequestBody UserDTO user) throws Exception {
        User updatedUser = userDao.findById(id).get();
        if (updatedUser.getStatus() == UserStatus.DELETED) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't delete a deleted user!");
        }
        if (user.getEmail() != null)
            updatedUser.setEmail(user.getEmail());
        if (user.getFirstName() != null)
            updatedUser.setFirstName(user.getFirstName());
        if (user.getMiddleName() != null)
            updatedUser.setMiddleName(user.getMiddleName());
        if (user.getLastName() != null)
            updatedUser.setLastName(user.getLastName());
        if (user.getBirthday() != null)
            updatedUser.setBirthday(user.getBirthday());
        if (user.getPassword() != null) {
            updatedUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        }
        userDao.save(updatedUser);
        return "Updated " + updatedUser.getFirstName() + "'s details.";
    }

    public User getUser(@PathVariable long id) {
        User user = userDao.findById(id).get();
        return user;
    }

    public String deleteUser(@PathVariable long id) throws Exception {
        User user = userDao.findById(id).get();
        String name = user.getFirstName() + " " + user.getLastName();
        if (user.getStatus() == UserStatus.DELETED) {
            throw new Exception("User already deleted");
        }
        user.setStatus(UserStatus.DELETED);
        userDao.save(user);
        return "Deleted user: " + name;
    }

    public String welcomeUser(Authentication authentication) {
        User currentUser = userDao.findByEmail(authentication.getName());
        String firstName = currentUser.getFirstName();
        String lastName = currentUser.getLastName();
        UserStatus status = currentUser.getStatus();
        LocalDateTime createdAt = currentUser.getCreatedAt();
        LocalDateTime updatedAt = currentUser.getUpdatedAt();

        return "Welcome, " + firstName + " " + lastName + "!"
                + "\n" + "User Status: " + status
                + "\n" + "Date Registered: " + createdAt
                + "\n" + "Last Updated: " + updatedAt;
    }
}
