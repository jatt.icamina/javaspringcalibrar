package com.example.Identity.enums;

public enum UserStatus {
    ACTIVE,
    DELETED
}
