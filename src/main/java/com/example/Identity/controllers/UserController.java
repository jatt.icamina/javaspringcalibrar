package com.example.Identity.controllers;

import com.example.Identity.dao.UserDao;
import com.example.Identity.model.User;
import com.example.Identity.model.UserDTO;
import com.example.Identity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@Valid @RequestBody UserDTO user) throws Exception {
        User existingUser = userDao.findByEmail(user.getEmail());
        if (existingUser != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already exists!");
        }
        return ResponseEntity.ok(userService.save(user));
    }

    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public ResponseEntity<?> getUsers() throws Exception {
        return ResponseEntity.ok(userService.getUsers());
    }

    @RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable long id) throws Exception {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUser(@PathVariable long id, @RequestBody UserDTO user) throws Exception {
        return ResponseEntity.ok(userService.updateUser(id, user));
    }

    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> deleteUser(@PathVariable long id) throws Exception {
        return ResponseEntity.ok(userService.deleteUser(id));
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ResponseEntity<?> welcomeUser(Authentication authentication) throws Exception {
        return ResponseEntity.ok(userService.welcomeUser(authentication));
    }
}
